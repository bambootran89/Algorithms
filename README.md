The ideas of algorithms from many sources but most from MIT course 6006 and http://wwwgeeksforgeeksorg/.

I implemented them in Java version.
let's practice more and more!!!

Algorithms:
+ Tree

    +AVL
	
    +BST
	
    +Heap
	
    +MaxHeap
	
    +MinHeap
	
    +SegmentTree
	
    +Tree
	
    +VerticalOrderBtree
	
+ LinkedList

    +LinkedList
	
+ Sorts

    +MergeSort
	
    +QuickSort
	
+ Hash

    +Cuckoo
	
    +itinerary
	
    +Largestsubarray0Sum
	
    +ModInverse
	
    +PalindromeSubstringQueries
	
    +SmallestRangeKList
	
    +SubarrayDistinctElements
	
+ Graph

    +BellmanFord
	
    +EulerianPath
	
    +FastDijkstras
	
    +Kruskal
	
    +StrongConnectedConponents
	
+ Optimization

    +DinicBlockMaxFlow
	
    +GlobalMinCut
	
    +MinCostMatching
	
    +MinCostMaxFlow
	
    +PushRelabelMaximumFlow
	
+ String

    +GeneralizedSuffixTree
	
    +kasai
	
    +LongestCommonSubstring
	
    +LongestPalindromicSubstring
	
    +LongestRepeatedSubstring
	
    +SearchingAllPatterns
	
    +SubstringCheck
	
    +SuffixTree
	
    +TestSuffixTree
	
    +Trie
	
+ DP:

    +BlackJack
	
    +GoSightSeeing
	
    +JobScheduling
	
    +Knapsack
	
    +Ksum
	
    +LargestSumContiguousSubarray
	
    +LCS
	
    +SubsetSum
	
    +TextJustification
	
    +VertexCoverTree
	
