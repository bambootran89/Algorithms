package trees;

public interface Tree {
	public void delete(int data);
	public int findheigh();
	public boolean search(int data);
	public void insert(int data);
}
